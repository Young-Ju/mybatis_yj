package com.greedy.section01;

import java.util.List;

public class PrintResult {

	public void printProdList(List<ProdDTO> prodList) {
		for(ProdDTO prod : prodList) {
			System.out.println(prod);
		}
	}
	
	public void printProd(ProdDTO prod) {
		System.out.println(prod);
	}

	public void printErrorMessage(String errorCode) {
		String errorMessage = "";
		switch(errorCode) {
		
		case "selectList" : errorMessage = "상품 조회 실패"; break;
		case "selectOne" : errorMessage = "상품 조회 실패"; break;
		case "insert" : errorMessage = "상품 등록 실패"; break;
		case "modify" : errorMessage = "상품 업데이트 실패"; break;
		case "delete" : errorMessage = "상품 삭제 실패"; break;
		}
		System.out.println(errorMessage);
	}

	public void printSuccessMessage(String successCode) {
		String successMessage = "";
		
		switch(successCode) {
		
		case "insert" : successMessage = "상품 등록 성공"; break;
		case "modify" : successMessage = "상품 수정 성공"; break;
		case "delete" : successMessage = "상품 삭제 성공"; break;
		}

		System.out.println(successMessage);
	
	
	}
	
	
	
	
	
	
	
	
	
	
	
}
