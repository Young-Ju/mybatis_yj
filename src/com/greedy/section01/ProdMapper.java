package com.greedy.section01;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.greedy.section01.ProdDTO;

public interface ProdMapper {

	List<ProdDTO> selectAllProd();

	//ProdDTO selectByProdCode(int code);
	
	List<ProdDTO> searchMenuByNameOrCategory(Map<String, Object> searchCriteria);
	
	int insertProd(ProdDTO prod);

	int modifyProd(ProdDTO prod);

	int deleteProd(int code);





}
