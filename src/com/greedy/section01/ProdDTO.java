package com.greedy.section01;

public class ProdDTO {
	
	private int prodCategory;
	private int prodCode;
	private String prodName;
	private int prodPrice;
	private int quantity;
	
	public ProdDTO() {}

	public ProdDTO(int prodCategory, int prodCode, String prodName, int prodPrice, int quantity) {
		super();
		this.prodCategory = prodCategory;
		this.prodCode = prodCode;
		this.prodName = prodName;
		this.prodPrice = prodPrice;
		this.quantity = quantity;
	}

	public int getProdCategory() {
		return prodCategory;
	}

	public void setProdCategory(int prodCategory) {
		this.prodCategory = prodCategory;
	}

	public int getProdCode() {
		return prodCode;
	}

	public void setProdCode(int prodCode) {
		this.prodCode = prodCode;
	}

	public String getProdName() {
		return prodName;
	}

	public void setProdName(String prodName) {
		this.prodName = prodName;
	}

	public int getProdPrice() {
		return prodPrice;
	}

	public void setProdPrice(int prodPrice) {
		this.prodPrice = prodPrice;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return "ProdDTO [prodCategory=" + prodCategory + ", prodCode=" + prodCode + ", prodName=" + prodName
				+ ", prodPrice=" + prodPrice + ", quantity=" + quantity + "]";
	}
	
	

}
