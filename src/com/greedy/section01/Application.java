package com.greedy.section01;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Application {

	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		MenuController menuController = new MenuController();
		//72/458s967d839.78
		
		do {
			System.out.println("====== 재고 관리 ======");
			System.out.println("1. 상품 전체 조회");
			System.out.println("2. 상품 코드로 상품 조회");
			System.out.println("3. 신규 상품 추가");
			System.out.println("4. 상품 수정");
			System.out.println("5. 상품 삭제");
			System.out.println("재고 관리 번호를 입력 : ");
			int no = sc.nextInt();
			
			switch(no) {
			
			case 1 : menuController.selectAllProd(); break;
			case 2 : MenuService.selectByProdCodeOrName(inputProdCode()); break;
			case 3 : menuController.insertProd(inputProd()); break;
			case 4 : menuController.modifyProd(inputModifyProd()); break;
			case 5 : menuController.deleteProd(inputProdDelete()); break;
			default : System.out.println("잘못된 메뉴를 선택했습니다.");
			}
		} while(true);

	}

	

	private static Map<String, Object> inputProdCode() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("검색할 조건을 입력하세요(category or name) : ");
		String condition = sc.nextLine();
		
		Map<String, Object> criteria = new HashMap<>();
		
		if("category".equals(condition)) {
			
			System.out.print("검색할 카테고리 코드 입력 : ");
			int categoryValue = sc.nextInt();
			
			criteria.put("categoryValue", categoryValue);
			
		} else if("name".equals(condition)) {
			
			System.out.print("검색할 메뉴 이름 입력 : ");
			String nameValue = sc.nextLine();
			
			criteria.put("nameValue", nameValue);
		} 
		
		return criteria;
	}
	
	
	
	private static Map<String, String> inputProd() {
		
		Scanner sc = new Scanner(System.in);
		System.out.print("상품 카테고리 : ");
		String category = sc.nextLine();
		System.out.print("상품 코드: ");
		String code = sc.nextLine();
		System.out.print("상품 이름 : ");
		String name = sc.nextLine();
		System.out.print("상품 가격: ");
		String price = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("category", category);
		parameter.put("code", code);
		parameter.put("name", name);
		parameter.put("price", price);
		
		return parameter;
	}

	
	private static Map<String, String> inputModifyProd() {
		
		Scanner sc = new Scanner(System.in);
		
		System.out.print("수정할 상품 이름 : ");
		String name = sc.nextLine();
		System.out.print("수정할 상품 가격: ");
		String price = sc.nextLine();
		
		System.out.print("수정할 상품 코드 : ");
		String code = sc.nextLine();
		
		Map<String, String> parameter = new HashMap<>();
		parameter.put("code", code);
		parameter.put("name", name);
		parameter.put("price", price);
		
		
		return parameter;
	}
	
	
	private static Map<String, String> inputProdDelete() {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("삭제할 상품 코드 입력 : ");
		String code = sc.nextLine();
		
		Map<String, String> parameter = new HashMap();
		parameter.put("code", code);
		
		
		return parameter;
	}
	
	
	
	
	
	
	
	
	
	
	
	
}
