package com.greedy.section01;

import static com.greedy.section01.Template.getSqlSession;


import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.greedy.section01.ProdDTO;
import com.greedy.section01.ProdMapper;

public class MenuService {
	
	private ProdMapper prodMapper;

	public List<ProdDTO> selectAllProd() {
		
		SqlSession sqlSession = getSqlSession();
		
		prodMapper = sqlSession.getMapper(ProdMapper.class);
		List<ProdDTO> prodList = prodMapper.selectAllProd();
		
		sqlSession.close();
		
		return prodList;
	}

	public ProdDTO selectByProdCode(int code) {
		
		SqlSession sqlSession = getSqlSession();
		prodMapper = sqlSession.getMapper(ProdMapper.class);
		
		ProdDTO prod = prodMapper.selectByProdCode(code);
		
		sqlSession.close();
		
		return prod;
	}

	public boolean insertProd(ProdDTO prod) {
		
		SqlSession sqlSession = getSqlSession();
		prodMapper = sqlSession.getMapper(ProdMapper.class);
		
		int result = prodMapper.insertProd(prod);
		
		if(result > 0) {
			
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}
	
	public boolean modifyProd(ProdDTO prod) {
		
		SqlSession sqlSession = getSqlSession();
		prodMapper = sqlSession.getMapper(ProdMapper.class);
		
		int result = prodMapper.modifyProd(prod);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}

	public boolean deleteprod(int code) {
		
		SqlSession sqlSession = getSqlSession();
		prodMapper = sqlSession.getMapper(ProdMapper.class);
		
		int result = prodMapper.deleteProd(code);
		
		if(result > 0) {
			sqlSession.commit();
		} else {
			sqlSession.rollback();
		}
		
		sqlSession.close();
		
		return result > 0 ? true : false;
	}



	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
