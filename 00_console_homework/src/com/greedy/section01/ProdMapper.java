package com.greedy.section01;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

public interface ProdMapper {

	List<ProdDTO> selectAllProd();

	ProdDTO selectByProdCode(int code);

	int insertProd(ProdDTO prod);

	int modifyProd(ProdDTO prod);

	int deleteProd(int code);





}
