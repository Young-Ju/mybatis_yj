package com.greedy.section01;

import java.util.List;
import java.util.Map;

public class MenuController {
	
	private final MenuService menuService;
	private final PrintResult printResult;
	
	public MenuController() {
		menuService = new MenuService();
		printResult = new PrintResult();
	}
	

	public void selectAllProd() {
		
		List<ProdDTO> prodList = menuService.selectAllProd();
		
		if(prodList != null) {
			printResult.printProdList(prodList);
		} else {
			printResult.printErrorMessage("selectList");
		}
		
		
	}

	public void selectByProdCode(Map<String, String> parameter) {
		
		int code = Integer.parseInt(parameter.get("code"));
		
		ProdDTO prod = menuService.selectByProdCode(code);
		
		if(prod !=null) {
			printResult.printProd(prod);
		} else {
			printResult.printErrorMessage("selectOne");		
		}
		
	}


	public void insertProd(Map<String, String> parameter) {
		
		ProdDTO prod = new ProdDTO();
		
		prod.setProdCategory(Integer.parseInt(parameter.get("category")));
		prod.setProdCode(Integer.parseInt(parameter.get("code")));
		prod.setProdName(parameter.get("name"));
		prod.setProdPrice(Integer.parseInt(parameter.get("price")));
		
		if(menuService.insertProd(prod)) {
			printResult.printSuccessMessage("insert");
		} else {
			printResult.printErrorMessage("insert");		
		}
		
	}


	public void modifyProd(Map<String, String> parameter) {
		
		ProdDTO prod = new ProdDTO();
		

		prod.setProdName(parameter.get("name"));
		prod.setProdPrice(Integer.parseInt(parameter.get("price")));
		prod.setProdCode(Integer.parseInt(parameter.get("code")));
		
		if(menuService.modifyProd(prod)) {
			printResult.printSuccessMessage("modify");
		} else {
			printResult.printErrorMessage("modify");
		}
	}


	public void deleteProd(Map<String, String> parameter) {
		int code = Integer.parseInt(parameter.get("code"));
		
		if(menuService.deleteprod(code)) {
			printResult.printSuccessMessage("delete");
		} else {
			printResult.printErrorMessage("delete");		
		}
	}


}
